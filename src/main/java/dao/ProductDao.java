/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author asus
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
         Connection con = null;
        Database db = Database.gerInstance();
        con=db.getCon();
        int id = -1;
        //process here
        try {
            String sql = "INSERT INTO PRODUCT (name,price) " +
                        "VALUES (?,? );"; 
            PreparedStatement stmt = con.prepareStatement(sql);     
            stmt.setString(1,object.getName());
            stmt.setDouble(2,object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            
            if(rs.next()){
                id=rs.getInt(1);
            }
        } catch (SQLException ex) {
             System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
         db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT;");
            while (rs.next()) {

                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT WHERE id=" + id);
            if (rs.next()) {

                int pid = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id, name, price);
                return product;
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
       Connection con = null;
        Database db = Database.gerInstance();
        con=db.getCon();
          int row=0;
        //process here
        try {
            String sql = "DELETE FROM PRODUCT  WHERE id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection con = null;
        Database db = Database.gerInstance();
        con=db.getCon();
        int row=0;
        //process here
        try {
            String sql = "UPDATE PRODUCT SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id=dao.add(new Product(-1,"Coffee",80));
        System.out.println("id: "+id);
        
        Product lastpro = dao.get(id);
        System.out.println("Last:"+lastpro);
        lastpro.setPrice(600);
        int row = dao.update(lastpro);
        
        Product udpro = dao.get(id);
        System.out.println("Update:"+udpro);
        dao.delete(id);
    }

}
