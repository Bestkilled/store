/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bestkilled.week01.poc;

import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.User;

/**
 *
 * @author asus
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1,"Thai Tea",30);
        Product p2 = new Product(2,"Green Tea",35);
        
        User seller = new User("Best","0202220000","password");
        Customer customer = new Customer("Fah","0202220000");
        
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 2);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 1);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 4);
        System.out.println(receipt);
    }
    
}
