/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class Database {

    private static Database instance = new Database();
    private Connection con;

    private Database() {

    }

    public static Database gerInstance() {
        String dbName = "./db/store.db";
        try {
            if (instance.con == null || instance.con.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.con = DriverManager.getConnection("jdbc:sqlite:" + dbName);
                System.out.println("Opened Successfully");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found");

        } catch (SQLException ex) {
            System.out.println("Unable to open db");

        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.con != null || !instance.con.isClosed()) {
                instance.con.close();

            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.con = null;
    }
    public static Connection getCon(){
        return instance.con;
    }
}
