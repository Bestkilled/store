/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bestkilled.week01.poc;

import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.gerInstance();
        con=db.getCon();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT;");
            while (rs.next()) {
                
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product product = new Product(id, name, price);
                
                System.out.println(product);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
             System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
    }
}
