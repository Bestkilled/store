/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author asus
 */
public class Receipt {

    private int id;
    private Date create;
    private User seller;
    private Customer customer;
    private ArrayList<Receiptdetail> receiptDetail;

    public Receipt(int id, Date create, User seller, Customer customer) {
        this.id = id;
        this.create = create;
        this.seller = seller;
        this.customer = customer;
        receiptDetail = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<Receiptdetail> getReceiptDetail() {
        return receiptDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id + ", create=" + create + ", seller=" + seller + ", customer=" + customer + ", total=" + this.getTotal() + '}' + "\n";
        for (Receiptdetail r : receiptDetail) {
            str = str + r.toString() + "\n";
        }
        return str;
    }

    public void setReceiptDetail(ArrayList<Receiptdetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    public Receipt(User seller, Customer customer) {
        this(-1, null, seller, customer);
    }

    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for(int row=0;row<receiptDetail.size();row++){
            Receiptdetail r =  receiptDetail.get(row);
            if(r.getProduct().getId()==product.getId()){
                r.addAmount(amount);
                return;
            }
            
        }
        receiptDetail.add(new Receiptdetail(id, product, amount, price, this));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount, product.getPrice());
    }
    public void deleteReceiptDetail(int row){
        receiptDetail.remove(row);
    }

    public double getTotal() {
        double total = 0;
        for (Receiptdetail d : receiptDetail) {
            total = total + d.getTotal();
        }
        return total;
    }
}
