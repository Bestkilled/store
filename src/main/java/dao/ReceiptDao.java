/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.Customer;
import Model.Product;
import Model.Receipt;
import Model.Receiptdetail;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        int id = -1;
        //process here
        try {
            String sql = "INSERT INTO RECEIPT (CUSTOMER_ID,USER_ID, TOTAL) VALUES (?, ?,?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
                object.setId(id);
            }
            for (Receiptdetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECEIPT_DETAIL (RECEIPT_ID, PRODUCT_ID,PRICE,AMOUNT)\n"
                        + "                           VALUES (?,?, ?, ? );";
                PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();

                ResultSet rsDetail = stmt.getGeneratedKeys();

                if (rsDetail.next()) {
                    id = rsDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        //process here
        Statement stmt = null;
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT r.ID as ID,r.CREATED as CREATED,CUSTOMER_ID,\n"
                    + "       c.NAME as CUSTOMER_NAME,\n"
                    + "       c.TEL as CUSTOMER_TEL,\n"
                    + "       USER_ID,\n"
                    + "       u.NAME as USER_NAME,\n"
                    + "       u.TEL as USER_TEL,\n"
                    + "       TOTAL\n"
                    + "  FROM RECEIPT r, CUSTOMER c, USER u\n"
                    + "  WHERE r.CUSTOMER_ID = c.ID AND r.USER_ID = u.ID"
                    + "  ORDER BY CREATED DESC;");
            while (rs.next()) {
                int id = rs.getInt("ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("CREATED"));
                int cusId = rs.getInt("CUSTOMER_ID");
                String cusName = rs.getString("CUSTOMER_NAME");
                String cusTel = rs.getString("CUSTOMER_TEL");
                int userId = rs.getInt("USER_ID");
                String userName = rs.getString("USER_NAME");
                String userTel = rs.getString("USER_TEL");
                double total = rs.getDouble("TOTAL");
                Receipt receipt = new Receipt(id, created, new User(userId, userName, userTel),
                        new Customer(cusId, cusName, cusTel));
                list.add(receipt);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        try {
            con.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        //process here 
        try {
            String sql = "SELECT r.ID as ID,CREATED,CUSTOMER_ID,\n"
                    + "       c.NAME as CUSTOMER_NAME,\n"
                    + "       c.TEL as CUSTOMER_TEL,\n"
                    + "       USER_ID,\n"
                    + "       u.NAME as USER_NAME,\n"
                    + "       u.TEL as USER_TEL,\n"
                    + "       TOTAL\n"
                    + "  FROM RECEIPT r, CUSTOMER c, USER u\n"
                    + "  WHERE r.ID = ? AND r.CUSTOMER_ID = c.ID AND r.USER_ID = u.ID;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int rid = rs.getInt("ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("CREATED"));
                int cusId = rs.getInt("CUSTOMER_ID");
                String cusName = rs.getString("CUSTOMER_NAME");
                String cusTel = rs.getString("CUSTOMER_TEL");
                int userId = rs.getInt("USER_ID");
                String userName = rs.getString("USER_NAME");
                String userTel = rs.getString("USER_TEL");
                double total = rs.getDouble("TOTAL");
                Receipt receipt = new Receipt(rid, created, new User(userId, userName, userTel),
                        new Customer(cusId, cusName, cusTel));
                getReceiptDetail(con, id, receipt);
                con.commit();
                return receipt;
            }
        } catch (SQLException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex1);
            }
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void getReceiptDetail(Connection con, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.ID as ID,\n"
                + "       RECEIPT_ID,\n"
                + "       PRODUCT_ID,\n"
                + "       p.NAME as PRODUCT_NAME,\n"
                + "       p.PRICE as PRODUCT_PRICE,\n"
                + "       rd.PRICE as PRICE,\n"
                + "       AMOUNT\n"
                + "  FROM RECEIPT_DETAIL rd, PRODUCT p\n"
                + "  WHERE RECEIPT_ID = ? AND rd.PRODUCT_ID = p.ID;";
        PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet rsDetail = stmtDetail.executeQuery();
        while (rsDetail.next()) {
            int idd = rsDetail.getInt("ID");
            int productID = rsDetail.getInt("PRODUCT_ID");
            String productName = rsDetail.getString("PRODUCT_NAME");
            double productPrice = rsDetail.getDouble("PRODUCT_PRICE");
            double price = rsDetail.getDouble("PRICE");
            int amount = rsDetail.getInt("AMOUNT");
            Product product = new Product(productID, productName, productPrice);
            receipt.addReceiptDetail(idd, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.gerInstance();
        con = db.getCon();
        int row = 0;
        //process here
        try {
            String sql = "DELETE FROM RECEIPT  WHERE id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection con = null;
//        Database db = Database.gerInstance();
//        con = db.getCon();
//        int row = 0;
//        //process here
//        try {
//            String sql = "UPDATE RECEIPT SET name = ?, price = ? WHERE id = ?;";
//            PreparedStatement stmt = con.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//            System.out.println("Affect row " + row);
//        } catch (SQLException ex) {
//            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
//        }
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Americano", 40);
        Product p2 = new Product(2, "Thai tea", 67);
        User seller = new User(1, "Plaiifah", "0800000000");
        Customer customer = new Customer(1, "Modpow", "0800000002");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 2);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("ID: " + dao.add(receipt));
        System.out.println("Receipt after add " + receipt);
        System.out.println("Get all " + dao.getAll());
        Receipt newDao = dao.get(receipt.getId());
        System.out.println("New Receipt :" + newDao);
        dao.delete(receipt.getId());
        System.out.println("Get all " + dao.getAll());

    }

}
