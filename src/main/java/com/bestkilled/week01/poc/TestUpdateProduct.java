/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bestkilled.week01.poc;

import Model.Product;
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author asus
 */
public class TestUpdateProduct {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.gerInstance();
        con=db.getCon();
        //process here
        try {
            String sql = "UPDATE PRODUCT SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product product = new Product(7, "Hot tea", 55);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            stmt.setInt(3, product.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
        }
        db.close();
    }
}
